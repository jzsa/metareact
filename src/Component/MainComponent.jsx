import React, { Component } from 'react';
import logo from '../logo.svg';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../App.css';


class MainComponent extends Component {

    render() {
        var style1 = { width: "75px", align: "left"};
        var style2 = { width: "150px" }
        if (this.props.uploads.length !== 0)
            var file = this.props.uploads.map((file, index) => {

                console.log("From component" + file);
                return (
                    <tr key={index} scope="row">
                        <th scop="row">{index + 1}</th>
                        <td style={{width: "300px"}}>{file} </td>
                    </tr>
                );
            });
        else {
            var file = <tr key={0} />;
        }

        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>Welcome to React</h2>
                </div>
                <p className="App-intro">
                    Word repetition calculator
              </p>
              <div style={{height: "350px"}}>
                <table align="center" className="table" style={{height: "75px", width: "300px"}}>
                    <thead className="thead-dark" style={{display: "block"}}>
                        <tr>
                            <th style={{width: "75px"}} scope="col">#</th>
                            <th style={{width: "300px"}} scope="col">files</th>
                        </tr>
                    </thead>
                    <tbody style={{display: "block", height:"300px", overflowY:"auto"}}>
                        {file}
                    </tbody>
                </table>
                </div>
                <table align="center">
                    <tbody>
                        <tr>
                            <td colSpan="3">
                                <form onSubmit={() => { console.log("submitted") }} >
                                    <input type="file" className="btn btn-success" style={{ padding: "10px 10px", margin: "20px 20px" }} onChange={(e) => { this.props.handleFiles(e.target.files) }} />
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span ><button onClick={this.props.onCalculate} className="btn btn-primary"> {"Sort"}</button></span>
                            </td>
                            <td>
                                <span ><button onClick={this.props.onDownload} className="btn btn-primary"> {"Download"}</button></span>
                            </td>
                            <td>
                                <span ><button onClick={this.props.clearFiles} className="btn btn-primary"> {"Clear"}</button></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default MainComponent;