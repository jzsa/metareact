import React, { Component } from 'react';
import MainComponent from '../Component/MainComponent';
import axios from 'axios';
// import Files from 'react-files';

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { uploads: [] }
  }

  componentDidMount=()=>
  {
    this.getFileList();
  }
  AddFile = (file) => {
    var data = new FormData();
    data.append("file", file);
    axios.post('http://localhost:8080/upload', data,
      {
        headers: {
          'Access-Control-Allow-Origin': 'http://localhost:8080/upload',
          'Content-Type': 'multipart/form-data'
        }
      })
      .then((response) => { this.setState({ uploads: response.data }); })
      .catch((error) => { console.log(error) });
    console.log(this.state)
    console.log("File added");
  }

  handleFiles = (files) => {
    this.AddFile(files[0]);
    // console.log(this.file.name);
  }

  handleCalculate = () => {
    axios.get('http://localhost:8080/start')
      .then((response) => { console.log(response); })
      .catch((error) => { console.log(error); })
      alert("Job done !")
  }

  handleDownload = () => {
    console.log("download called");
    axios({
      url: 'http://localhost:8080/download',
      method: 'GET',
      responseType: 'blob', // important
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'output.zip');
      document.body.appendChild(link);
      link.click();
    })
  }

  clear = () => {
    axios.get('http://localhost:8080/clear')
      .then((response) => { this.setState({ uploads: response.data }); })
      .catch((error) => { console.log(error); })
  }

  getFileList = () => {
    axios.get('http://localhost:8080/list')
      .then((response) => { this.setState({ uploads: response.data }); })
      .catch((error) => { console.log(error); })
  }

  render() {
    return (
      <div>
        <MainComponent onDownload={this.handleDownload}
          onAddFile={this.handleAddFile}
          handleFiles={this.handleFiles}
          uploads={this.state.uploads}
          clearFiles={this.clear}
          onCalculate={this.handleCalculate} />
      </div>
    );
  }
}

export default MainContainer;