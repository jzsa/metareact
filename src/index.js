import React from 'react';
import ReactDOM from 'react-dom';
import MainContainer from './Container/MainContainer';
import './index.css';

ReactDOM.render(
  <MainContainer />,
  document.getElementById('root')
);
